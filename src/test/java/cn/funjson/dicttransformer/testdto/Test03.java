package cn.funjson.dicttransformer.testdto;

import cn.funjson.dicttransformer.compoment.DictKey;
import lombok.Data;

/**
 * @author fengjiansong
 * @date 2021/8/9 10:19 上午
 */
@Data
public class Test03 {

    @DictKey(valueName = "value03",group = "3",dynamicHookClass = TestCustomDataHook.class)
    private String key3="1";

    private String value03;

}
