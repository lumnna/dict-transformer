package cn.funjson.dicttransformer.testdto;

import cn.funjson.dicttransformer.hook.CustomDataHook;
import org.springframework.stereotype.Service;

/**
 * @author 82062
 */
@Service
public class TestCustomDataHook2 extends CustomDataHook {

    @Override
    public Object lazyInitLocalData(int groupId, Object hashKey) {
        return "奥里给2";
    }
}
