package cn.funjson.dicttransformer.testdto;

import cn.funjson.dicttransformer.compoment.DictKey;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * @author fengjiansong
 * @date 2021/8/9 10:19 上午
 */
@Data
public class Test02 {

    @DictKey(valueName = "value02",group = "3",dynamicHookClass = TestCustomDataHook.class)
    private String key2="1";

    private String value02;

    private List<Test03> testLists=new ArrayList<>();

}
