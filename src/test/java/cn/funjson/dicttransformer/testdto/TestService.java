package cn.funjson.dicttransformer.testdto;

import cn.funjson.dicttransformer.compoment.EnableDict;
import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * @author fengjiansong
 * @date 2021/8/9 10:25 上午
 */
@Service
@Slf4j
public class TestService {

    @EnableDict
    public Test01 testList(){

        Test01 test01 = new Test01();
        Test02 test02=new Test02();
        Test03 test03 = new Test03();

        test02.getTestLists().add(test03);
        test01.getTestLists().add(test02);

        log.info("使用前：{}",JSONObject.toJSONString(test01));
        return test01;

    }

}
