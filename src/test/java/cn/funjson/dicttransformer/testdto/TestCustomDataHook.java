package cn.funjson.dicttransformer.testdto;

import cn.funjson.dicttransformer.hook.CustomDataHook;
import org.springframework.stereotype.Service;

/**
 * @author 82062
 */
@Service
public class TestCustomDataHook extends CustomDataHook {

    @Override
    public Object lazyInitLocalData(int groupId, Object hashKey) {

        return "奥里给";
    }
}
