package cn.funjson.dicttransformer.testdto;

import cn.funjson.dicttransformer.compoment.DictKey;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * @author fengjiansong
 * @date 2021/8/9 10:19 上午
 */
@Data
public class Test01 {

    @DictKey(valueName = "userName",group = "3",dynamicHookClass = TestCustomDataHook.class)
    @DictKey(valueName = "dept",group = "3",dynamicHookClass = TestCustomDataHook2.class)
    private Integer id=1;

    private String userName;

    private String dept;

    private List<Test02> testLists=new ArrayList<>();

}
