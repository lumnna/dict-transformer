package cn.funjson.dicttransformer.testdto;

import cn.funjson.dicttransformer.compoment.DictKey;
import lombok.Data;

/**
 * @author fengjiansong
 * @date 2021/8/9 10:19 上午
 */
@Data
public class Test04 {

    @DictKey(valueName = "value04",group = "3")
    private String key4="1";

    private String value04;


}
