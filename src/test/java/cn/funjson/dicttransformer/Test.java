package cn.funjson.dicttransformer;

import cn.funjson.dicttransformer.testdto.TestService;
import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = TestProjectApplication.class)
@Slf4j
public class Test {

    @Resource
    private TestService testService;

    @org.junit.Test
    public void testMethod(){
        log.info(JSONObject.toJSONString(testService.testList()));
    }

}
