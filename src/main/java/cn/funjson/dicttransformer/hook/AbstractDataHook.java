package cn.funjson.dicttransformer.hook;

import cn.funjson.dicttransformer.model.CacheModel;
import lombok.var;

import java.util.HashMap;

/**
 * 动态数据钩子
 * @author fengjs
 */
public abstract class AbstractDataHook {

    /**
     * 查询数据源
     * @param groupId 预留字段
     * @param hashKey 查询的key
     * @return 数据
     */
    public abstract Object lazyInitLocalData(int groupId,Object hashKey);


    protected void addLocalCache(CacheModel cacheModel, Integer groupId, String hashKey, String value){

        var innerMap = cacheModel.getCache().get(groupId);

        if (innerMap==null){
            innerMap=new HashMap<>();
            innerMap.put(hashKey,value);
            cacheModel.getCache().put(groupId,innerMap);
        }else{
            innerMap.put(hashKey,value);
        }

    }

}
