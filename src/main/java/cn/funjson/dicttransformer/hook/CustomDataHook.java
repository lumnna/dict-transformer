package cn.funjson.dicttransformer.hook;

import cn.funjson.dicttransformer.model.CacheModel;
import cn.funjson.dicttransformer.utils.DictTransformerUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

/**
 * @author  fengjiansong
 * @date  2021-08-09 22:27
 * @version v2.0
 */
public class CustomDataHook extends AbstractDataHook{

    private final String DEFAULT = "MOCK";

    /**
     * 通过查询惰性初始化本地数据源
     */
    @Override
    public Object lazyInitLocalData(int groupId, Object hashKey){

        return DEFAULT;
    }

    @Override
    protected void addLocalCache(CacheModel cacheModel, Integer groupId, String hashKey, String value) {
        super.addLocalCache(cacheModel, groupId, hashKey, value);
    }

}
