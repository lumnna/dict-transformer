package cn.funjson.dicttransformer.hook;

import cn.funjson.dicttransformer.utils.DictTransformerUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

/**
 * @author  fengjiansong
 * @date  2021-08-09 22:27
 * @version v2.0
 */
@Component
@ConditionalOnProperty(prefix = DictTransformerUtils.DICT_PREFIX, name = "useMock",matchIfMissing = true)
@ConditionalOnMissingBean(CustomDataHook.class)
@Order(2)
@Slf4j
public class MockDataHook extends AbstractDataHook{

    private final String DEFAULT = "MOCK";

    /**
     * 通过查询惰性初始化本地数据源
     */
    @Override
    public Object lazyInitLocalData(int groupId, Object hashKey){

        return DEFAULT;
    }


}
