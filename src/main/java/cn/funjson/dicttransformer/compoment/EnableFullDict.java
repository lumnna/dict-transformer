package cn.funjson.dicttransformer.compoment;

import org.aspectj.lang.annotation.DeclareAnnotation;

import java.lang.annotation.*;

/**
 * @author fengjiansong
 * @date 2021/4/27 2:45 下午
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD})
public @interface EnableFullDict {

    boolean request() default true;

    boolean response() default true;

}
