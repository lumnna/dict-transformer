package cn.funjson.dicttransformer.compoment;

import java.lang.annotation.*;

/**
 * 开启字典转换能力注解
 * @author fengjiansong
 * @date 2021/4/27 2:45 下午
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD})
public @interface EnableDict {

    boolean value() default true;

}
