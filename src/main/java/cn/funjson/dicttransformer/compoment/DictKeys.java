package cn.funjson.dicttransformer.compoment;

import cn.funjson.dicttransformer.hook.AbstractDataHook;

import java.lang.annotation.*;

/**
 * DictKey注解容器
 * {@link DictKey}
 * @author fengjiansong
 * @date 2021/8/10 21:45
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD})
public @interface DictKeys {

    DictKey[] value() default {};

}
