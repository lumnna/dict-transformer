package cn.funjson.dicttransformer.compoment;

import cn.funjson.dicttransformer.hook.AbstractDataHook;
import cn.funjson.dicttransformer.hook.CustomDataHook;

import java.lang.annotation.*;

/**
 * 字典转换标识注解
 * @author fengjiansong
 * @date 2021/4/27 2:45 下午
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD})
@Repeatable(DictKeys.class)
public @interface DictKey {

    String valueName() default "";

    String group() default "";

    Class<? extends AbstractDataHook> dynamicHookClass() default AbstractDataHook.class;

    String[] value() default {};
}
