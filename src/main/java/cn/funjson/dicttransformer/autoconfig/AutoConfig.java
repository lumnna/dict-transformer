package cn.funjson.dicttransformer.autoconfig;

import cn.funjson.dicttransformer.aspect.DickAspect;
import cn.funjson.dicttransformer.hook.MockDataHook;
import cn.funjson.dicttransformer.model.CacheModel;
import cn.funjson.dicttransformer.utils.SpringUtil;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

/**
 * @author  fengjiansong
 * @date  2021/5/8 5:41 下午
 */
@Configuration
@Import({SpringUtil.class,CacheModel.class, MockDataHook.class, DickAspect.class})
public class AutoConfig {

}
