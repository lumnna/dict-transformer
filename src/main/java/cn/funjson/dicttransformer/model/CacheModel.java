package cn.funjson.dicttransformer.model;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @author fengjiansong
 * @date  2021/5/8 5:19 下午
 */
@Data
@Component
@Slf4j
public class CacheModel {

    private Map<Integer, Map<String,String>> cache=new HashMap<>();

    private final SimpleDateFormat SPT =new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    @Value("${dict.delay:1000}")
    private Long dictDelay;

    @Value("${dict.repeat:300000}")
    private Long dictRepeat;

    @PostConstruct
    private void forkResetCacheThread(){
        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                resetCache();
            }
        },dictDelay,dictRepeat);
    }

    private void resetCache(){
        log.info("重制dict本地缓存 时间：{}",SPT.format(new Date()));
        assert cache != null;
        cache=new HashMap<>(16);
    }

}
