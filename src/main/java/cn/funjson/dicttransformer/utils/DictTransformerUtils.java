package cn.funjson.dicttransformer.utils;

/**
 * @author fengjiansong
 * @date 2021/5/24 5:45 下午
 */
public class DictTransformerUtils {
    /**
     * The prefix of property name of dict-transformer
     */
    public static final String DICT_PREFIX = "dict";
}
