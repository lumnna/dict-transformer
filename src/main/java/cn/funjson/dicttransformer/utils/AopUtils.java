package cn.funjson.dicttransformer.utils;

/**
 * @author fengjiansong
 * @date 2021/4/28 6:08 下午
 */
public class AopUtils {

    /**
     * 判断是否是用户自定义对象
     * @param clazz 判断对象class
     * @return boolean true java自己的对象 false 用户自己的对象
     */
    public static boolean isJavaClass(Class<?> clazz) {
        return clazz != null && clazz.getClassLoader() == null;
    }



}
