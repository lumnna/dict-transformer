## 字典转换服务
### 1. 介绍：
#### 1. 目的：
1. 字典转换服务的最终目的是提高开发编码效率，是一款提效的工具。
2. 字典转换服务最一开始的开发初衷从名字上也能很容易看的出来是为了以AOP的方式做字典表的自动转换的，但是现在这个版本已经不止针对字典表转换的情况了，而是面向用户业务的所有场景的一个转换。
#### 2. 使用场景：
在我们日常开发中经常会遇到类似要对返回结构体其中的某些字段进行转换(很明显的类似字典表数据key-value这种数据；像这种数据有一些特点，比如说数据离你的查询比较远不能很简单的去连表查询转换，或者说我只是因为需求变更简单的加了一个展示字段，但是这个字段对我程序的逻辑影响很大，改动代码很麻烦)，这时候我们在处理数据的时候要么硬连表查询或者在内存中遍历结果集去转换。
#### 3. 支持范围：
    可转换的范围：
    1. 任意嵌套深度的用户类型实体类
    2. 任意嵌套深度的复杂类型(集合嵌套)
    3. 集合类型只支持List并且要标注范型
#### 4. 注意事项：
    1.要注意嵌套深度(量机器和方法而行)
    2.所有日志为debug级别，要调试的话要打开debug开关
    3.默认启用的MockDataHook以保证用户在不使用的情况下程序正常
    4.所以用户要正常使用此服务的话要扩展CustomDataHook
    5.建议：@Dictkey注解给定dynamicHookClass属性方便以后扩展数据源
    6.注意：dynamicHookClass属性要么全都默认要么全都指定，有多个扩展CustomDataHook的情况下，全都要指定。
### 2. 使用：
#### 1. maven引入jar包：
release分支自行下载源码构建jar包，其他的服务maven引入即可
#### 2. 标注注解：
    1.实体类注解
        @DictKey
            valueName default "" 当前类中要赋值的属性名
            group default "" 预留字段
            dynamicHookClass default AbstractDataHook.class 数据源的实现
            value default {} repeat容器用的
    2.方法注解
        @EnableDict
            value default true 为此方法开启返回值字典转换能力
#### 3. 扩展数据源实现查询逻辑：
继承CustomDataHook重写lazyInitLocalData方法
此方法入参group为扩展字段，hashkey为标记注解的值
此方法返回的数据为赋值
#### 4. 高级用法：
    可以在一个属性上重复打注解，实现一对多转换
### 3. 目录结构：
看源码吧，累了不想写了 - -｜
### 4. 使用示范：
源码中test包内为使用示范
### 5. 效果展示
1. 使用前的返回的结构体
```
{
    "id":1,
    "testLists":[
        {
            "key2":"1",
            "testLists":[
                {
                    "key3":"1"
                }
            ]
        }
    ]
}
```
2. 使用后的返回的结构体
```
{
    "dept":"奥里给2",
    "id":1,
    "testLists":[
        {
            "key2":"1",
            "testLists":[
                {
                    "key3":"1",
                    "value03":"奥里给"
                }
            ],
            "value02":"奥里给"
        }
    ],
    "userName":"奥里给"
}
```
3. service
```
/**
 * @author fengjiansong
 * @date 2021/8/9 10:25 上午
 */
@Service
@Slf4j
public class TestService {

    @EnableDict
    public Test01 testList(){

        Test01 test01 = new Test01();
        Test02 test02=new Test02();
        Test03 test03 = new Test03();

        test02.getTestLists().add(test03);
        test01.getTestLists().add(test02);

        log.info("使用前：{}",JSONObject.toJSONString(test01));
        return test01;

    }

}

```
4. dto
```
/**
 * @author fengjiansong
 * @date 2021/8/9 10:19 上午
 */
@Data
public class Test01 {

    @DictKey(valueName = "userName",group = "3",dynamicHookClass = TestCustomDataHook.class)
    @DictKey(valueName = "dept",group = "3",dynamicHookClass = TestCustomDataHook2.class)
    private Integer id=1;

    private String userName;

    private String dept;

    private List<Test02> testLists=new ArrayList<>();

}
```
5. customDataHook扩展
```
@Service
public class TestCustomDataHook extends CustomDataHook {

    @Override
    public Object lazyInitLocalData(int groupId, Object hashKey) {

        return "奥里给";
    }
}
```
### 6. feature：
1. 会考虑增加payloud限制序列化以及反序列化深度
2. 有可能会考虑注解内变为参数数组，全面放开查询